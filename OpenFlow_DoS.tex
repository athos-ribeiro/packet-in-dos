\documentclass[12pt]{article}

\usepackage{sbc-template}

\usepackage{graphicx,url}

%\usepackage[brazil]{babel}   
%\usepackage[latin1]{inputenc}  
\usepackage[utf8]{inputenc}

% included for pseudo algorithm
\usepackage{amsmath}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}

     
\sloppy

\title{An Algorithm to respond to PacketIn DoS attacks through behavior change}

\author{Athos Coimbra Ribeiro\inst{1}, Luiz Arthur dos Santos\inst{2}, Fabio Kon\inst{1}, Daniel Macêdo Batista\inst{1}}

\address{Instituto de Matemática e Estatística -- Universidade de São Paulo (IME-USP)\\
  Rua do Matão, 1010 -- Cidade Universitária -- 05508-090 -- Sao Paulo, SP -- Brazil
  \nextinstitute
    Departamento Acadêmico de Computação\\
    Universidade Tecnológica Federal do Paraná (UTFPR)\\
    Campo Mourão -- PR
  \email{\{athoscr,kon,batista\}@ime.usp.br, luizsantos@utfpr.edu.br}
}

\makeatletter
\def\BState{\State\hskip-\ALG@thistlm}
\makeatother

\begin{document} 

\maketitle

\begin{abstract}
Software Defined Networking controllers may become the target of Denial of
Service attacks, since their availability may determine the whole network's
availability as well. Often, small networks will not provide redundancy for
these controllers, making the controller a single point of failure.
For the OpenFlow protocol, these attacks may occur due to packet\_in
packets flooding. The present work proposes an algorithm to respond to
Denial of Service attacks targeting OpenFlow controllers by monitoring the
controller resources and changing from a reactive behavior to a proactive one
whenever under attack. Results from experiments show that switching the network
behavior during an attack can effectively stop the attacker from bringing the
network down.
\end{abstract}
     
\section{Introduction}

Conducting research on networks through measurements may be a challenging task: network devices, such as switches, usually come with proprietary software, making it harder to modify embedded software shipped with the hardware. Even in the cases they can be modified, replicating the experiment would depend on specific hardware, ending up in new ideas neither being tested nor being tried \cite{whitepaper}.  
By decoupling the control logic of such devices, Software Defined Networking (SDN) allows scientists and practitioners to develop new technology \cite{shin} \cite{batista2015perspectives} and due to open SDN protocols and their free software implementations, people are now able to produce replicable experiments using real network devices instead of simulations. In fact, several companies, such as Google \cite{google}, are finding that SDN is a viable option to provide scalability to their cloud services.

OpenFlow is a popular protocol for Software Defined Networking \cite{salsano}. It separates the control plane from network devices, like switches and routers, and allows them to edit entries in a \textit{flow table} in the device data plane changing the way a given set of packets would be handled at a given time.

One of OpenFlow's applicability is to improve network security through detection \cite{shin} (e.g., extracting statistics from an OpenFlow switch to verify traffic patterns and classify a set of packets as threat) and mitigation (e.g., changing the flow table to drop malicious packets on the switch before reaching its target). A possible scenario would be to detect Denial of Service (DoS) attacks against a server behind the OpenFlow network and add a flow entry to block the packets related to that attack, so they would never be forwarded to the server under attack.

Related work \cite{shin} shows that efforts to mitigate Denial of Service attacks to a host behind an OpenFlow network, as mentioned before, may fail if the fact that these attacks may affect or even target the OpenFlow controller is ignored.

This paper presents an algorithm to stop Denial of Service attacks targeting\footnote{A DoS attack may affect the OpenFlow controller even when not directly targeting it. This paper also consider these attacks.} OpenFlow controllers on small networks, where no levels of redundancy are added to the controller, by detecting and mitigating the attack. It also includes deployment of test environments using existing software and implementation of the mitigation approach.

The remainder of this paper is structured as follows: Section \ref{related_work} discusses a few studies related to Network Security using OpenFlow that experienced or discussed the lack of resources in an OpenFlow controller, leading to decreases in the network availability. Section \ref{relevant_concepts} presents a brief explanation on the concepts surrounding this work. In Section \ref{hypothesis}, we present an approach to detect and mitigate the DoS attacks by monitoring the OpenFlow controller resources and switching from a reactive network behavior to a proactive one whenever under attack and apply the methodology presented in Section \ref{methodology} to conduct experiments over the proposed approach. The results are then presented in Section \ref{results}. Finally, Section \ref{conclusion} compares the results of this work with related works and proposes future research.

\section{Related Work}\label{related_work}

Santos, Campiolo, and Batista \cite{luizarthur} have shown
that it is possible to integrate Intrusion Detection Systems with Software Defined Networking to create a system capable of mitigating certain attacks on local networks, such as avoiding port scanning and blocking Denial of Service (DoS) attacks. \cite{braga} also proposes a system using the OpenFlow protocol which uses Self Organized Maps to classify network traffic as normal or abnormal based on statistics extracted from the OpenFlow switch, warning the network administrator of the possible threat. This administrator could then set rules to add new entries to the flow table to block these attacks. \cite{shin} proposes some modifications both in the OpenFlow controller and switch implementations to, among other things, block Denial of Service attacks in the switch using the SYN Cookie technique \cite{eddy2007rfc} and loading conditional flow rules in the OpenFlow switch, which requires OpenFlow implementation modifications. The authors also realized that SYN flood attacks targeting a host inside an SDN, may flood the controller, making not only the targeted host unavailable, but also the OpenFlow controller and consequently, the rest of the network. Note that \cite{luizarthur} partially observed this fact.

In the end of \cite{luizarthur}, the authors propose investigating how to better protect the OpenFlow controller, since the technique presented aims to protect the hosts behind the network, not the controller itself. \cite{braga} experienced stress on the OpenFlow controller while conducting experiments to compare the proposed approach with other similar approaches. It would happen whenever a high rate of DoS attacks with IP Header spoofing \cite{cisco} was performed, which happens due to the high traffic between the OpenFlow controller and the switch in order to get new entries for the flow table, since the packets do not match any of the entries in the table due to the IP spoofing techniques. \cite{shin} was able to completely stop SYN flood \cite{rfc_syn_flood} DoS attacks, but deploying solutions in hardware through modifying the OpenFlow implementation makes future updates harder or even impossible \cite{braga}. \cite{shin} also points that only TCP SYN flood attacks were studied, leaving UDP and ICMP attacks for future works.

To the best of our knowledge, there are no works
proposing solutions for DoS attacks on reactive SDNs that do not
provide redundancy for the OpenFlow controller. Changing the behavior of an SDN from a reactive to a proactive context to mitigate these attacks seems to be an acceptable approach for the problem.

\section{Relevant Concepts}\label{relevant_concepts}

\subsection{Software Defined Networking and OpenFlow}

Software Defined Networking is an approach to separate the decision making layer, the \textbf{control plane}, from the systems that forward traffic to specific destinations, \textbf{the data plane}. Classic forwarding network devices provide these two layers, control plane and data plane, embedded in the hardware. Possible ways of setting these devices are shown in Figure \ref{fig1}: the device on the left has its control plane set through an application which uses a communication protocol defined by the manufacturer, while the one on the right is set through a console. It means that whenever changes in the network are needed, one must do it in the device, which usually has proprietary software, making it hard to develop third party applications to manage the devices. When the network has many devices from different manufacturers, making changes on the network becomes a complex activity. By decoupling data and control plane, as shown in Figure \ref{fig2}, Software Defined Networks allow network administrators to perform changes on the networks by programming in through the controller, abstracting the hardware.

% FIGURA 1 classic network
\begin{figure}
  \centering
  \includegraphics[width=0.9\textwidth]{images/classic_nwk.png}
  \caption{Classic network devices}
  \label{fig1}
\end{figure}

% FIGURA 2 SDN
\begin{figure}
  \centering
  \includegraphics[width=0.75\textwidth]{images/sdn.png}
  \caption{Software Defined Networking}
  \label{fig2}
\end{figure}

OpenFlow \cite{whitepaper} is an open protocol for Software Defined
Networking that provides a remote controller to modify the behavior
of OpenFlow compatible network devices. It can match packets in layers
1 to 4, including MAC addresses, IP fields and TCP/UDP headers. It
defines how the controller and the network devices should behave and
also defines a communication protocol, Figure \ref{fig3} summarizes
how OpenFlow divides the control plane from the data plane. The former
is now a computer, the controller, while the later is still in the
network device. When an \textbf{OpenFlow switch} receives a packet, it
tries to match it against its \textbf{flow tables}, if it matches, the
switch applies an \textbf{action}, related to the matching entry in
the table, to the packet, like forwarding the packet to a port or dropping
it. If the packet does not match with any of the entries in the flow
table, the switch sends that packet encapsulated in a
\textbf{packet\_in} packet to the \textbf{OpenFlow controller}. A
packet\_in consists of a header, a buffer\_id to track the buffered
packet, the length of the captured packet, the port in which the packet was received, and a data field with the captured packet. The controller analyzes the packet and may either:

\begin{enumerate}
  \item reply to the switch with an \textbf{ACK} packet and drop the captured packet;
  \item reply with a packet\_out packet, which may contain the captured packet or the information about in which buffer the captured packet is located so it can be forwarded to its destiny (it could also specify some other action for the captured packet).
  \item reply with a flow\_mod packet, which tells the switch to add a new entry to the flow table and reprocess the packet that generated the packet\_in message, this time applying the new rule inserted in the flow table. Now, similar packets get a match in the table without the need to get forwarded to the controller again.
\end{enumerate}

OpenFlow based networks may be configured in a \textbf{proactive} or in a \textbf{reactive} mode: in the former, the flow tables are always filled with several rules, reducing the communication with the controller to a minimum. In the latter, the tables are constantly updated according to the network traffic.

% FIGURA 3 OpenFlow
\begin{figure}
  \centering
  \includegraphics[width=0.9\textwidth]{images/openflow.png}
  \caption{OpenFlow}
  \label{fig3}
\end{figure}

\subsection{Denial of Service attacks}

Denial of Service (DoS) attacks are attempts to make a resource, usually a host or network device, unavailable to its users. A packet\_in flood attack is a Denial of service attack targeting an OpenFlow controller, which is illustrated in Figure \ref{fig4}: when a packet does not match an entry in the flow table in the OpenFlow switch, a packet\_in packet is sent to the controller with the unmatched packet. If the adversary finds a way to send several packets that will not match entries in the flow table, the switch will keep sending packets to the controller, which will analyze these packets and respond. Flooding the network with these unmatched packets, may lead to unavailability of the controller and, for reactive setups without controller redundancy, the whole network may get unavailable.

% FIGURA 4 packet_in flood attack
\begin{figure}
  \centering
  \includegraphics[width=0.9\textwidth]{images/packet_in.png}
  \caption{1) Adversary sends packet; 2) Packet does not match  any rule in the flow table; 3) Switch sends packet\_in to the controller}
  \label{fig4}
\end{figure}

\section{A mechanism to stop packet\_in flood attacks}\label{hypothesis}

A packet\_in flood attack will not affect a proactive OpenFlow network
if the switches match 100\% of the packets. This happens because if
every packet gets a match in the flow tables, no packet\_in packets
will be sent to the controller. In order to match all packets, a rule
to drop all packets that will not match any other rule would be
needed, making the network 100\% proactive. This scenario might not be
desirable, since the reactive features of the OpenFlow networks might be
useful to the user. One could then keep a list with all the non-abnormal flows usually installed in the switches and, whenever under packet\_in attacks, install the \textbf{normal flows} list to the switches for a period of time, temporarily changing from a reactive (or hybrid) context to a proactive context. The algorithm of this mechanism is presented in Algorithm \ref{alg}.

\begin{algorithm}
  \caption{SDN Context Swap}\label{euclid}
  \begin{algorithmic}[1]
    \Procedure{ContextSwap}{}
    \State $\textit{t} \gets \text{duration for proactive mode}$
    \State $\textit{list} \gets \text{list of entries for the flow table during proactive mode}$
    \State $\textit{defined\_limit} \gets \text{CPU usage limit to trigger proactive mode}$ \\
    \BState \emph{BEGIN}:
    \State \Call{start\_reactive\_mode}{\null}
    \While{$\Call{get\_cpu\_usage}{\null} <= \textit{defined\_limit}$}
      \State \text{Continue}
    \EndWhile \\
    \State \Call{start\_proactive\_mode}{\textit{t}, \textit{list}}
    \State \textbf{goto} \emph{BEGIN}.
    \EndProcedure
  \end{algorithmic}
  \label{alg}
\end{algorithm}

In large production networks, installing such lists could be unfeasible due to the number of flows that could be considered normal. But in such networks, there are levels of redundancy for the OpenFlow controller, and this approach, as mentioned earlier, is focused on small OpenFlow based networks, where listing and installing most of the normal flows can be (easily) done.

The proposed approach can avoid unavailability of the network whenever under a packet\_in flood attack, but it does not identify the attack. The problem with packet\_in attacks is the high consumption of the controller's processing capabilities. Monitoring the CPU usage and the traffic of packet\_in packets sent to the controller, we can trigger a context switch (change from reactive to proactive behavior) and stop the Denial of Service attack whenever the CPU usage and the incoming packet\_in packets reach some pre-defined values.

\section{Methodology}\label{methodology}

A clean experimental environment was deployed, using the structure present in a  public repository dedicated to this project, available at \url{https://gitlab.com/athos-ribeiro/of-controller-dos}. The repository also contains the specific setups for network topology and OpenFlow applications. The content in the repository is available under the GPL license. The environment consists of a virtual machine running mininet to simulate the OpenFlow switches, the hosts, servers, and adversaries in the Software Defined Network and another virtual machine running POX \cite{pox} to simulate the OpenFlow controller. A few tools were used to perform the attacks and monitor the network, including a Perl script to perform UDP flood attacks, a Python function to monitor the CPU usage, embedded in the POX functions, and Wireshark to sniff the packets sent to the controller.  Monitoring and attack simulations were done in the machine running mininet, since we did not find any reason to isolate neither the attacker nor the monitoring machine. To facilitate replication and reproducibility, the environment deployment was automated using Chef \cite{chef}.

Figure \ref{fig5} shows the network topology used for the experiments, which consisted of a controller, two switches (s1 and s2), two adversaries, and one client connected to s1 and a target and a web server, both connected to s2.

%topology
\begin{figure}
  \centering
  \includegraphics[width=0.9\textwidth]{images/topology.png}
  \caption{Network topology}
  \label{fig5}
\end{figure}

\subsection{Environment Calibration}

Before initiating the experiments, we wanted to find a scenario where the controller could not process new incoming packets, so the whole network would stop responding. One case in which we achieved this calibration goal was the one shown in Figure \ref{fig5}. The environment was calibrated with packet\_in flood attacks. This was done by sending UDP packets from the two attackers to random ports of the target machine while the DoS prevention system was turned off, until the OpenFlow controller could not update the flow tables in the OpenFlow switches, compromising the availability of the network. The availability was tested by running a web server in the host machine connected to the s2 switch and trying to retrieve HTML content from the client machine connected to the s1 switch (ICMP packets were also used during calibration phases).

\subsection{Prevention System}
The prevention system was implemented in the controller machine, using its interfaces to retrieve information and install flows in the switches. The CPU usage monitoring system retrieves the information of CPU usage for an instant and puts it in a fixed sized queue (removing the oldest element in the structure) making it possible for the prevention system to use a moving average of the CPU usage in the last $n$ units of time, as shown in Equation \ref{eq1}, where $C_1$ to $C_n$ are the reading of CPU usage on instants $1$ to $n$, and $n$ is the total number of readings done this way.

\begin{equation}
  SMA = \frac{C_1 + C_2 + ... + C_n}{n}
  \label{eq1}
\end{equation}

The implemented DoS prevention system was then turned on and the attacks were performed again, as in the calibration step, for a given amount of time.

\section{Results}\label{results}

As shown in Figure \ref{fig6}, the topology proposed on Figure \ref{fig5} could successfully consume 100\% of the controller CPU. Other topologies tested also achieved the same percentage when attacking the controller, the difference is in the web server (also shown in Figure \ref{fig5}) availability. On the chosen topology, the web server would not respond the requests coming from the host on the left of Figure \ref{fig5}, which was the desired calibration.

\begin{figure}
  \centering
  \includegraphics[width=0.9\textwidth]{images/cpu_sem_prevencao.png}
  \caption{CPU usage during calibrations}
  \label{fig6}
\end{figure}

After implementing the algorithm shown in Algorithm \ref{alg}, with a Moving average with $n = 4$ and time frame of $t = 0.3$ seconds, a CPU limit of 75\% was set. With this settings, the web server would always respond to the host, even during the attacks. The controller CPU usage is shown in Figure \ref{fig7}. The spikes in the graph are due to the short time of proactive behavior, which was set to last for 10 seconds. After that, the network would start behaving as a reactive network again and would become vulnerable to the packet\_in attacks once more. Note that the web server is still available to the host because TCP connections to its port 80 is in the list of \textbf{normal traffic} that is pushed to the switch, when assuming a proactive behavior. If a second web server on the right side of Figure \ref{fig5} was added, without being listed in the \textbf{normal traffic} list, it would not be available during the attacks.

\begin{figure}
  \centering
  \includegraphics[width=0.9\textwidth]{images/cpu_com_prevencao.png}
  \caption{CPU usage during attacks with prevention system triggered at 75\% usage}
  \label{fig7}
\end{figure}

\section{Conclusion and future direction}\label{conclusion}

The algorithm presented showed itself effective for OpenFlow networks without redundancy for the controller. A list of normal traffic is needed to keep the network available, which is feasible in the context of small networks. Stop behaving as a reactive network for short periods seems to be a reasonable price to stop DoS attacks, as long as one can map most of the services behind the network to keep it functioning as a proactive network.

It is possible to obtain the normal traffic list through machine learning techniques, by analyzing the packets sent in packet\_in packets when the network is not under attack, which we left for future work. Also, the act of blocking flows might get less restrictive on each iteration to identify the nature of the attacks, like the source of the attacks and which kind of attack is being used. Finally, another interesting future work would be to use machine learning techniques to search for the pattern of the packages used in the attack and collect data on the attacks. 

\bibliographystyle{sbc}
\bibliography{OpenFlow_DoS}

\end{document}
