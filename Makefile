MAIN=OpenFlow_DoS.tex
AUX=OpenFlow_DoS.aux

all:
	pdflatex $(MAIN)
	bibtex $(AUX)
	pdflatex $(MAIN)
	pdflatex $(MAIN)

.PHONY: clean

clean:
	rm -f *.aux *.bbl *.blg *.log *.pdf
